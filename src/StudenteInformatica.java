public class StudenteInformatica extends Studente {
    private String corsoDiStudi;


    public String getCorsoDiStudi() {
        return corsoDiStudi;
    }

    public void setCorsoDiStudi(String corsoDiStudi) {
        this.corsoDiStudi = corsoDiStudi;
    }

    public StudenteInformatica(String nome,int matricola,String corsoDiStudi){
        super(nome,matricola);
        this.corsoDiStudi=corsoDiStudi;
    }

    @Override
    public void stampaInfo(){
        super.stampaInfo();
        System.out.println("Corso di studi: " +getCorsoDiStudi());
    }
}
