public class Studente {

    private String nome;
    private int matricola;

    public Studente(String nome, int matricola){
        this.nome=nome;
        this.matricola=matricola;
    }

    public int getMatricola() {
        return matricola;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public void stampaInfo(){
        System.out.println("Nome Studente: " +getNome() + "\nMatricola: " +getMatricola());
    }
}
