//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Studente studente1=new Studente("Anna", 841189);
        studente1.stampaInfo();

        StudenteInformatica studente2= new StudenteInformatica("Alessandro",851177,"Informatica");
        studente2.stampaInfo();

        StudenteMatematica studente3= new StudenteMatematica("Pio",881157,"Matematica");
        studente3.stampaInfo();
    }
}